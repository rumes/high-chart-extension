import { TestBed } from '@angular/core/testing';

import { HighChartExtentionService } from './high-chart-extention.service';

describe('HighChartExtentionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HighChartExtentionService = TestBed.get(HighChartExtentionService);
    expect(service).toBeTruthy();
  });
});
