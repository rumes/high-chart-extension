import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { element } from '@angular/core/src/render3';
import { PointDetails } from './high-chart/models/point-details';
import * as Chart from 'highcharts';

@Component({
  selector: 'hce-high-chart-extention',
  template: `
    <hce-high-chart [element] = "chart1" [conf] = "chartConf"></hce-high-chart>
  `,
  styles: []
})
export class HighChartExtentionComponent implements OnInit {
  @Input() element: string;
  @Input() conf: Chart.Options;
  @Output() pointClick: EventEmitter<PointDetails>;

  public chart1: string ;
  public chartConf: Chart.Options;

  constructor() {
   }

  ngOnInit() {
    this.chart1 = this.element;
    this.chartConf = this.conf;
  }

}
