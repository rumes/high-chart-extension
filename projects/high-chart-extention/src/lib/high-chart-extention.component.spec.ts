import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HighChartExtentionComponent } from './high-chart-extention.component';

describe('HighChartExtentionComponent', () => {
  let component: HighChartExtentionComponent;
  let fixture: ComponentFixture<HighChartExtentionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HighChartExtentionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HighChartExtentionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
