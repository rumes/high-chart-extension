import { NgModule } from '@angular/core';
import { HighChartExtentionComponent } from './high-chart-extention.component';
import { HighChartComponent } from './high-chart/high-chart.component';

@NgModule({
  declarations: [HighChartExtentionComponent, HighChartComponent],
  imports: [
  ],
  exports: [HighChartExtentionComponent]
})
export class HighChartExtentionModule { }
