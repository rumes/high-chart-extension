import { Component, OnInit, Input, ElementRef, ViewChild, Renderer2, Output, EventEmitter, AfterViewInit } from '@angular/core';
import * as Chart from 'highcharts';
import { element } from 'protractor';
import { PointDetails } from './models/point-details';

@Component({
  selector: 'hce-high-chart',
  templateUrl: './high-chart.component.html',
  styleUrls: ['./high-chart.component.css']
})
export class HighChartComponent implements OnInit, AfterViewInit {
  @Input() element: string;
  @Input() conf: Chart.Options;

  @Output() pointClick: EventEmitter<PointDetails>;

  // @ViewChild() chartElement: ElementRef;

  public chart: Chart.ChartObject;

  public containerElement: ElementRef;

  // public chartOption: Chart.ChartOptions;
  public chartOption: Chart.Options;

  constructor(private renderer: Renderer2, private el: ElementRef) {
   }

  ngOnInit() {
    this.chartOption = this.conf;
    console.log('in init', this.chartOption);
    const div: ElementRef = this.renderer.createElement('div', 'chart');
    this.renderer.setAttribute(div, 'id', this.element);
    this.renderer.appendChild(this.el.nativeElement, div);
  }

  ngAfterViewInit(): void {
    console.log('in ngAfterViewInit');
    this.chart = Chart.chart(this.el.nativeElement.querySelector('div'), this.chartOption );
  }

}
