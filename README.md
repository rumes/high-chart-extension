# High Chart Extention

This is angular extention for [highchart js](https://www.highcharts.com/) you can simply install using npm and use it features in angular app.

## Requirements
1. npm 
2. angular 7.0 or higher.
3. highchart 6.2.0 or higher.

## Installation
> npm i high-chart-extention

## Usage
After npm installation you can use it in your angular application. Before use it in your component you have import HighChartExtentionModule to your **module.ts**.

### app.module.ts
```javascript
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HighChartExtentionModule } from 'high-chart-extention';
import { from } from 'rxjs';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HighChartExtentionModule <------ include the HighChartExtentionModule in imports.
  ],
  providers: [],
  bootstrap: [AppComponent] 
})
export class AppModule { }
```

then you can use it in your component html file

### component.html
```html
<hce-high-chart-extention id="chart" [conf]="chartOption"></hce-high-chart-extention>
```
you have to add two input to html element to create chart using high-chart-extention.

1. **id** = *this will be the id of chart container div.*
2. **conf** = *this is the configuration object. It is highchart Option object.*

    ex: 
    ```javascript
        import { Component } from "@angular/core";
        import { Options } from "highcharts";

        @Component({
            selector: "app-root",
            templateUrl: "./app.component.html",
            styleUrls: ["./app.component.css"]
        })
        export class AppComponent {
            public chartOption: Options;
            title = "high-chart-extention-app";

            constructor() {
                this.chartOption = {
                chart: {
                    type: "line"
                },
                title: {
                    text: "Test Chart"
                },
                xAxis: {
                    categories: ["Apples", "Bananas", "Oranges"]
                },
                yAxis: {
                    title: {
                    text: "Fruit eaten"
                    }
                },
                series: [
                    {
                    name: "Jane",
                    data: [1, 0, 4]
                    },
                    {
                    name: "John",
                    data: [5, 7, 3]
                    }
                ]
                };
            }
        }
    ```

