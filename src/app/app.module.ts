import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HighChartExtentionModule } from 'high-chart-extention';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HighChartExtentionModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
