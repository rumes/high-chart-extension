import { Component } from "@angular/core";
import { Options } from "highcharts";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  public chartOption: Options;
  title = "high-chart-extention-app";

  constructor() {
    this.chartOption = {
      chart: {
        type: "line"
      },
      title: {
        text: "Test Chart"
      },
      xAxis: {
        categories: ["Apples", "Bananas", "Oranges"]
      },
      yAxis: {
        title: {
          text: "Fruit eaten"
        }
      },
      series: [
        {
          name: "Jane",
          data: [1, 0, 4]
        },
        {
          name: "John",
          data: [5, 7, 3]
        }
      ]
    };
  }
}
